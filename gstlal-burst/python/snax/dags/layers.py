# Copyright (C) 2020  Patrick Godwin (patrick.godwin@ligo.org)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import itertools
import os

from ligo.segments import segment, segmentlist

from gstlal import plugins
from gstlal.dags import Argument, Option
from gstlal.dags.layers import Layer, Node
from gstlal.datafind import DataCache

from gstlal.snax import utils
from gstlal.snax.feature_extractor import DataType


def extract_features_layer(config, dag):
	layer = Layer(
		"gstlal_snax_extract",
		requirements={"request_cpus": 2, "request_memory": 5000, **config.condor.submit},
		transfer_files=config.condor.transfer_files,
	)

	common_args = [
		Option("data-source", "frames"),
		Option("frame-type", f"{config.ifo}={config.source.frame_type[config.ifo]}"),
		Option("sample-rate", config.features.sample_rate),
		Option("mismatch", config.features.mismatch),
		Option("waveform", config.features.waveform),
		Option("q-high", config.features.q_high),
		Option("psd-fft-length", config.psd.fft_length),
		Option("cadence", config.output.dataset_cadence),
		Option("persist-cadence", config.output.file_cadence),
		Option("frame-segments-name", config.source.frame_segments_name),
		Option("data-find-server", config.source.data_find_server),
	]

	if "frequency_bins" in config.features:
		common_args.append(Option("frequency-bin", config.features.frequency_bins))

	feature_cache = DataCache(DataType.SNAX_FEATURES)
	for span, subset in itertools.product(config.time_bins[config.all_ifos], config.channel_bins):
		start, end = span
		features_start = start + config.features.start_pad

		channel_subset = config.channel_groups[int(subset)]
		channel_names = format_channel_names(config.channels, channel_subset)

		job_span = segment(features_start, end)
		features = DataCache.generate(
			DataType.SNAX_FEATURES,
			config.all_ifos,
			utils.split_segments_by_stride(
				job_span,
				config.segments[config.ifo],
				config.output.file_cadence
			),
			svd_bins=subset,
		)
		feature_cache += features

		layer += Node(
			arguments = [
				Option("job-id", subset),
				Option("channel-name", channel_names),
				Option("feature-start-time", features_start),
				Option("gps-start-time", start),
				Option("gps-end-time", end),
				*common_args,
			],
			inputs = [
				Option("frame-segments-file", config.source.frame_segments_file),
			],
			outputs = Argument("features", features.files, suppress=True),
		)

	dag.attach(layer)
	return feature_cache


def combine_features_layer(config, dag, feature_cache):
	layer = Layer(
		"gstlal_snax_combine",
		requirements={"request_cpus": 1, "request_memory": 4000, **config.condor.submit},
		transfer_files=config.condor.transfer_files,
	)

	combined_feature_cache = DataCache.generate(
		DataType.SNAX_FEATURES,
		config.all_ifos,
		config.time_bins,
	)

	for (start, end), features in combined_feature_cache.groupby("time").items():
		features_start = start + config.features.start_pad
		span = segment(features_start, end)
		features = feature_cache.groupby_bins("time", [span])[span]

		layer += Node(
			arguments = [
				Option("start-time", features_start),
				Option("end-time", end),
				Option("instrument", config.ifo),
				Option("tag", "offline"),
			],
			inputs = Argument("features", features.files, suppress=True),
			outputs = Option("outdir", config.output.directory),
		)

	dag.attach(layer)
	return combined_feature_cache


def extract_features_online_layer(config, dag):
	layer = Layer(
		"gstlal_snax_extract",
		requirements={"request_cpus": 2, "request_memory": 8000, **config.condor.submit},
		transfer_files=config.condor.transfer_files,
		retries=1000,
	)

	# set up datasource options
	if config.source.data_source == "framexmit":
		datasource_args = [
			Option("data-source", "framexmit"),
			Option("framexmit-addr", f"{config.ifo}={config.source.framexmit_addr[config.ifo]}"),
			Option("framexmit-iface", config.source.framexmit_iface),
		]
	elif config.source.data_source == "lvshm":
		datasource_args = [
			Option("data-source", "lvshm"),
			Option("shared-memory-partition", f"{config.ifo}={config.source.shared_memory_partition[config.ifo]}"),
			Option("shared-memory-block-size", config.source.shared_memory_block_size),
			Option("shared-memory-assumed-duration", config.source.shared_memory_assumed_duration),
		]
	else:
		raise ValueError(f"data source = {config.source.data_source} not valid for online jobs")

	common_args = [
		Option("sample-rate", config.features.sample_rate),
		Option("mismatch", config.features.mismatch),
		Option("waveform", config.features.waveform),
		Option("q-high", config.features.q_high),
		Option("psd-fft-length", config.psd.fft_length),
		Option("kafka-server", config.services.kafka_server),
		Option("kafka-partition", config.stream.kafka_partition),
		Option("kafka-topic", config.stream.kafka_topic),
	]

	feature_cache = DataCache.generate(
		DataType.SNAX_FEATURES,
		config.all_ifos,
		svd_bins=config.channel_bins
	)

	for subset, features in feature_cache.groupby("bin").items():
		channel_subset = config.channel_groups[int(subset)]
		channel_names = format_channel_names(config.channels, channel_subset)

		layer += Node(
			arguments = [
				Option("job-id", subset),
				Option("channel-name", channel_names),
				*common_args,
				*datasource_args,
			]
		)

	dag.attach(layer)
	return feature_cache


def synchronize_features_layer(config, dag, feature_cache):
	layer = Layer(
		"gstlal_snax_synchronize",
		requirements={"request_cpus": 1, "request_memory": 4000, **config.condor.submit},
		transfer_files=config.condor.transfer_files,
		retries=1000,
	)

	layer += Node(
		arguments = [
			Option("tag", config.tag),
			Option("num-topics", len(feature_cache.groupby("bin").keys())),
			Option("kafka-server", config.services.kafka_server),
			Option("processing-cadence", config.stream.processing_cadence),
			Option("request-timeout", config.stream.request_timeout),
			Option("latency-timeout", config.stream.latency_timeout),
			Option("input-topic-basename", config.stream.kafka_topic),
			Option("output-topic-basename", f"synchronizer_{config.tag}")
		]
	)

	dag.attach(layer)


def save_features_layer(config, dag, feature_cache):
	layer = Layer(
		"gstlal_snax_sink",
		requirements={"request_cpus": 1, "request_memory": 4000, **config.condor.submit},
		transfer_files=config.condor.transfer_files,
		retries=1000,
	)

	layer += Node(
		arguments = [
			Option("tag", config.tag),
			Option("instrument", config.ifo),
			Option("kafka-server", config.services.kafka_server),
			Option("input-topic-basename", f"synchronizer_{config.tag}"),
			Option("processing-cadence", config.stream.processing_cadence),
			Option("request-timeout", config.stream.request_timeout),
			Option("features-path", config.output.directory),
			Option("channel-list", config.source.channel_list),
			Option("safety-include", config.source.safety_include),
			Option("unsafe-channel-include", config.source.unsafe_channel_include),
			Option("waveform", config.features.waveform),
			Option("sample-rate", config.features.sample_rate),
			Option("write-cadence", config.output.dataset_cadence),
			Option("persist-cadence", config.output.file_cadence),
		]
	)

	dag.attach(layer)


def format_channel_names(channel_dict, channels):
	"""
	given a channel dict and a list of a channels, format
	channel names needed within gstlal_snax_extract
	"""
	return [f"{channel}:{channel_dict[channel]['fsamp']}" for channel in channels]


@plugins.register
def layers():
	return {
		"extract": extract_features_layer,
		"combine": combine_features_layer,
		"extract_online": extract_features_online_layer,
		"synchronize": synchronize_features_layer,
		"save": save_features_layer,
	}
