# Copyright (C) 2010  Kipp Cannon (kipp.cannon@ligo.org)
# Copyright (C) 2010  Chad Hanna (chad.hanna@ligo.org)
# Copyright (C) 2018  Ryan Magee (ryan.magee@ligo.org)
# Copyright (C) 2020  Patrick Godwin (patrick.godwin@ligo.org)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## @file

## @package segutil

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


import itertools
import math

from ligo import segments


#
# =============================================================================
#
#                              Segment Utilities
#
# =============================================================================
#


def analysis_segments(ifos, allsegs, boundary_seg, start_pad = 0, overlap = 0, min_instruments = 1, one_ifo_length = (3600 * 8)):
	"""get a dictionary of all the disjoint detector combination segments
	"""
	ifos = set(ifos)
	segsdict = segments.segmentlistdict()

	# segment length dependent on the number of instruments
	# so that longest job runtimes are similar
	segment_length = lambda n_ifo: one_ifo_length / 2 ** (n_ifo - 1)

	# generate analysis segments
	for n in range(min_instruments, 1 + len(ifos)):
		for ifo_combos in itertools.combinations(list(ifos), n):
			ifo_key = frozenset(ifo_combos)
			segsdict[ifo_key] = allsegs.intersection(ifo_combos) - allsegs.union(ifos - set(ifo_combos))
			segsdict[ifo_key] = segsdict[ifo_key].protract(overlap)
			segsdict[ifo_key] &= segments.segmentlist([boundary_seg])
			segsdict[ifo_key] = split_segments(segsdict[ifo_key], segment_length(len(ifo_combos)), start_pad)
			if not segsdict[ifo_key]:
				del segsdict[ifo_key]

	return segsdict


def split_segments_by_lock(ifos, seglistdicts, boundary_seg, max_time = 10 * 24 * 3600):
	ifos = set(seglistdicts)

	# create set of segments for each ifo when it was
	# in coincidence with at least one other ifo
	doublesegs = segments.segmentlistdict()
	for ifo1 in ifos:
		for ifo2 in ifos - set([ifo1]):
			if ifo1 in doublesegs:
				doublesegs[ifo1] |= seglistdicts.intersection((ifo1, ifo2))
			else:
				doublesegs[ifo1] = seglistdicts.intersection((ifo1, ifo2))

	# This is the set of segments when at least two ifos were on
	doublesegsunion = doublesegs.union(doublesegs.keys())

	# This is the set of segments when at least one ifo was on
	segs = seglistdicts.union(seglistdicts.keys())

	# define when "enough time" has passed
	def enoughtime(seglist, start, end):
		return abs(seglist & segments.segmentlist([segments.segment(start, end)])) > 0.7 * max_time

	# iterate through all the segment where at least one ifo was on and extract
	# chunks where each ifo satisfies our coincidence requirement. A consequence is
	# that we only define boundaries when one ifos is on
	chunks = segments.segmentlist([boundary_seg])

	# This places boundaries when only one ifo or less was on
	for start, end in doublesegsunion:
		if all([enoughtime(s, chunks[-1][0], end) for s in doublesegs.values()]):
			chunks[-1] = segments.segment(chunks[-1][0], end)
			chunks.append(segments.segment(end, boundary_seg[1]))

	# check that last segment has enough livetime
	# if not, merge it with the previous segment
	if len(chunks) > 1 and abs(chunks[-1]) < 0.3 * max_time:
		last_chunk = chunks.pop()
		chunks[-1] = segments.segmentlist([chunks[-1], last_chunk]).coalesce().extent()

	return chunks


def split_segments(seglist, maxextent, overlap):
	newseglist = segments.segmentlist()
	for bigseg in seglist:
		newseglist.extend(split_segment(bigseg, maxextent, overlap))
	return newseglist


def split_segment(seg, maxextent, overlap):
	if maxextent <= 0:
		raise ValueError("maxextent must be positive, not %s" % repr(maxextent))

	# Simple case of only one segment
	if abs(seg) < maxextent:
		return segments.segmentlist([seg])

	# adjust maxextent so that segments are divided roughly equally
	maxextent = max(int(abs(seg) / (int(abs(seg)) // int(maxextent) + 1)), overlap)
	maxextent = int(math.ceil(abs(seg) / math.ceil(abs(seg) / maxextent)))
	end = seg[1]

	seglist = segments.segmentlist()

	while abs(seg):
		if (seg[0] + maxextent + overlap) < end:
			seglist.append(segments.segment(seg[0], seg[0] + maxextent + overlap))
			seg = segments.segment(seglist[-1][1] - overlap, seg[1])
		else:
			seglist.append(segments.segment(seg[0], end))
			break

	return seglist
