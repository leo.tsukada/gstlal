#!/usr/bin/env python3
#
# Copyright (C) 2018, 2019 Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import argparse
import getpass
import os

from gstlal.dags import Argument, Option, DAG
from gstlal.dags.layers import Layer, Node


parser = argparse.ArgumentParser(description = 'generate a dt dphi covariance matrix and tree data to replace share/inspiral_dtdphi_pdf.h5')
parser.add_argument('--psd-xml', help = 'XML containing HLVK psd')
parser.add_argument('--H-snr', type = float, help = 'H characteristic SNR')
parser.add_argument('--L-snr', type = float, help = 'L characteristic SNR')
parser.add_argument('--V-snr', type = float, help = 'V characteristic SNR')
parser.add_argument('--K-snr', type = float, help = 'K characteristic SNR')
parser.add_argument('--m1', type = float, default = 1.4, help = 'primary component mass')
parser.add_argument('--m2', type = float, default = 1.4, help = 'secondary component mass')
parser.add_argument('--s1', type = float, default = 0., help = 'primary (z) spin')
parser.add_argument('--s2', type = float, default = 0., help = 'secondary (z) spin')
parser.add_argument('--flow', type = float, default = 10., help = 'Low frequency cut-off. Default 10 Hz')
parser.add_argument('--fhigh', type = float, default = 1024., help = 'High frequency cut-off. Default 1024 Hz')
args = parser.parse_args()

if args.flow >= args.fhigh:
    raise ValueError("flow cannot be greater than fhigh")

dag = DAG("dt_dphi")
dag.create_log_dir()

requirements = {
	"want_graceful_removal": "True",
	"kill_sig": "15",
	"accounting_group_user": getpass.getuser(),
	"accounting_group": "ligo.prod.o3.cbc.uber.gstlaloffline",
}

cov_layer = Layer(
	"gstlal_inspiral_compute_dtdphideff_cov_matrix",
	requirements={"request_cpus": 1, "request_memory": 2000, **requirements},
)
marg_layer = Layer(
	"gstlal_inspiral_create_dt_dphi_snr_ratio_pdfs",
	requirements={"request_cpus": 1, "request_memory": 7000, **requirements},
)
add_layer = Layer(
	"gstlal_inspiral_add_dt_dphi_snr_ratio_pdfs",
	requirements={"request_cpus": 1, "request_memory": 4000, **requirements},
)

cov_layer += Node(
	arguments = [
		Option("H-snr", args.H_snr),
		Option("L-snr", args.L_snr),
		Option("V-snr", args.V_snr),
		Option("K-snr", args.K_snr),
		Option("flow", args.flow),
		Option("fhigh", args.fhigh),
		Option("m1", args.m1),
		Option("m2", args.m2),
		Option("s1", args.s1),
		Option("s2", args.s2),
	],
	inputs = Option("psd-xml", args.psd_xml),
	outputs = Option("output", "covmat.h5"),
)
dag.attach(cov_layer)

num = 1000
marg_files = []
# FIXME dont hardcode 3345408, it comes from number of tiles in TimePhaseSNR
for start in range(0, 3345408, num):
	stop = start + num
	marg_file = f"inspiral_dtdphi_pdf_{start:d}_{stop:d}.h5"
	marg_layer += Node(
		arguments = [Option("start", start), Option("stop", stop)],
		inputs = Option("cov-mat", "covmat.h5"),
		outputs = Option("output", marg_file),
	)
	marg_files.append(marg_file)
dag.attach(marg_layer)

add_layer += Node(
	inputs = Argument("inputs", ["covmat.h5"] + marg_files),
	outputs = Argument("inspiral_dtdphi_pdf.h5", suppress=True)
)
dag.attach(add_layer)

dag.write_dag("dt_dphi.dag")
dag.write_script("dt_dphi.sh")
